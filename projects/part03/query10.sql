.read data.sql

select H1.name, H1.grade, H2.name, H2.grade from Highschooler H1, Highschooler H2 where (H1.ID, H2.ID) in
	(
		select Likes.ID1, Likes.ID2 from Likes 
		inner join Highschooler as H1 on Likes.ID1 = H1.ID
		inner join Highschooler as H2 on Likes.ID2 = H2.ID
		where H1.grade - H2.grade >= 2
	);