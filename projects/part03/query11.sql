.read data.sql


select name, grade from Highschooler where ID in
	(
		select Likes.ID1 from Likes where (Likes.ID1, Likes.ID2) in 
			(
				select L1.ID1, L1.ID2 from Likes as L1
				inner join Likes as L2 
				on L1.ID1 = L2.ID2 and L1.ID2 = L2.ID1
			)
			and
			(
				(
					(Likes.ID1, Likes.ID2) in (select * from Friend)
				)
				or
				(
					(Likes.ID2, Likes.ID1) in (select * from Friend)
				)
			)
	)
	order by grade, name;