.read data.sql



insert into Friend (ID1, ID2)
	select distinct F1.ID1, F2.ID2 from Friend as F1
	inner join Friend as F2 on F1.ID2 = F2.ID1
	where F1.ID1 <> F2.ID2
	and not exists(select ID1, ID2 from Friend where F1.ID1 = ID1 and F2.ID2 = ID2)
	and not exists(select ID1, ID2 from Friend where F1.ID1 = ID2 and F2.ID2 = ID1)
	union
	select distinct F3.ID2, F4.ID1 from Friend as F3
	inner join Friend as F4 on F4.ID2 = F3.ID1
	where F4.ID1 <> F3.ID2
	and not exists(select ID1, ID2 from Friend where F3.ID1 = ID1 and F4.ID2 = ID2)
	and not exists(select ID1, ID2 from Friend where F3.ID1 = ID2 and F4.ID2 = ID1);