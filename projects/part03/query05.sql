.read data.sql

select HS.Name, HS.Grade from Highschooler as HS where 
  not exists
	(
		select * from Highschooler as HS2
		join Friend as F1 on HS2.ID = F1.ID2 
		where F1.ID1 = HS.ID and HS.Grade = HS2.Grade
	)
	and not exists
		(
			select * from Highschooler as HS3 
			join Friend as F2 on HS3.ID = F2.ID1 
			where F2.ID2 = HS.ID and HS.Grade = HS3.Grade
		);
