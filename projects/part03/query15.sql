.read data.sql

select distinct HS1.Name, HS1.Grade, HS2.Name, HS2.Grade, HS3.Name, HS3.Grade from Highschooler as HS1, Highschooler as HS2, Highschooler as HS3
where (HS1.ID, HS2.ID, HS3.ID) in
	(
		select F1.ID1, F1.ID2, F2.ID2 from Friend as F1 inner join Friend as F2 on F1.ID2 = F2.ID1 where (F1.ID1, F2.ID2) in
		(
				select ID1, ID2 from Likes where (ID1, ID2) not in
					(
						select ID1, ID2 from Friend
					)
						and (ID2, ID1) not in
					(
						select ID1, ID2 from Friend
					)
		)
		or
		(F2.ID2, F1.ID1) in
		(
			select ID1, ID2 from Likes where (ID1, ID2) not in
				(
					select ID1, ID2 from Friend
				)
					and (ID2, ID1) not in
				(
					select ID1, ID2 from Friend
				)
		)
	)
	or (HS1.ID, HS2.ID, HS3.ID) in
	(
		select F1.ID2, F1.ID1, F2.ID1 from Friend as F1 inner join Friend as F2 on F1.ID1 = F2.ID2
		where (F1.ID2, F2.ID1) in
			(
					select ID1, ID2 from Likes where (ID1, ID2) not in
						(
							select ID1, ID2 from Friend
						)
							and (ID2, ID1) not in
						(
							select ID1, ID2 from Friend
						)
			)
		or
		(F2.ID2, F1.ID1) in
			(
				select ID1, ID2 from Likes where (ID1, ID2) not in
					(
						select ID1, ID2 from Friend
					)
						and (ID2, ID1) not in
					(
						select ID1, ID2 from Friend
					)
			)
	);
