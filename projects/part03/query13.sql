.read data.sql


select H1.name, H1.grade, H2.name, H2.grade from Highschooler as H1, Highschooler as H2
where (H1.ID, H2.ID) in
	(
		select ID1, ID2 from Likes where ID2 not in
			(
				select L.ID1 from Likes as L
			)
	);