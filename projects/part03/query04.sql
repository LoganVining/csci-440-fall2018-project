.read data.sql


select H1.name, H1.grade, H2.name, H2.grade, H3.name, H3.grade from Highschooler as H1, Highschooler as H2, Highschooler as H3 where (H1.ID, H2.ID, H3.ID) in
	(
		select L1.ID1, L1.ID2, L2.ID2 from Likes as L1 inner join Likes as L2 on L1.ID2 = L2.ID1
		where L1.ID1 <> L2.ID2
	);