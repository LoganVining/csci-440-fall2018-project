# Team Members

* Logan Vining
* Glen Johnson

# Mapping Relationships from an ER Diagram to a Relational Model
    The goal of this tutorial would be to teach readers effective techniques for mapping relationships in ER Diagrams to tables in a Relational Model.
    Specifically, the tutorial would explain the three approaches to mappin 1:1 relationship types (foreign key approach, merged relation, approach, and cross-referencing approach). The tutorial would then discuss how to map 1:N and N:1 relationship types, again using the foreign key approach or the cross-referencing approach. Finally, the tutorial would discuss and provide examples of mapping M:N relationship types using the cross-referencing approach. Hopefully, the readers of this tutorial would firstly learn the most effective and efficient techniques to map relationships of an ER diagram to a Relational Model, but more importantly, that readers learn the importance of stepping through the process of taking an ER Diagram, mapping it to a Relational Model, and finally turning that model into a usable database.

# Insert, Update, & Delete Anomalies
    The goal of this tutorial would be to inform readers firstly about what each of these anomalies are and secondly, how to avoid creating these types of anomalies in a database. Specifically, this tutorial would discuss how these anomalies come about relating to database design and the best techniques for avoiding them during the database design process. This tutorial is especially important in that it highlights the importance of proper database design and the importance of taking the time to design databases correctly to save time and resources in the future.

# Building Complex SQL Statements
    The goal of this tutorial would be to walk the user through examples of constructing complex statements in SQL, such as insert statements that must use internal select statements or creating custom tuples with multiple select statements. This tutorial would hopefully serve as a guide to make readers' lives easier when constructing complex statements in SQL, as it would explain the best ways to break down the query into smaller parts, and steadily build up the query parts separately until they can come together to complete the entire query. This tutorial holds value in the fact that it would simplify the process of constructing complex statements while stepping through examples to highlight the important points to keep in mind and to not get overwhelmed by trying to face a complex query all at once.
